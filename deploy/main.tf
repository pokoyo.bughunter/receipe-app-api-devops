terraform {
  backend "s3" {
    bucket  = "recipe-app-api-tfstates"
    key     = "recipe-app-api.tfstates"
    region  = "us-east-1"
    encrypt = true
    #dynamodb_table = "recipe-app-api-tfstates-lock"
    #key = "LockId"
  }
}


terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.29.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region = "us-east-1"
}


locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.maintainer
    ManagedBy   = var.managedby
  }
}
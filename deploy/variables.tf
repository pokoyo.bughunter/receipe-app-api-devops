variable "prefix" {
  default = "instance"
}

variable "project" {
  default = "receipe-app-api"
}

variable "bastion_name" {
  default = "bastion-host"
}

variable "maintainer" {
  default = "Ksanchez@sappotech"
}

variable "managedby" {
  default = "terraform"
}


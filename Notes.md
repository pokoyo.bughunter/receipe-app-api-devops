# Setup AWS-VAULT
- brew install aws-vault
- choco install aws-vault

# Configure AWS-VAULT & AWS Profile
- aws-vault.exe add iac
- Add [profile iac] session To ~/.aws/config
- Go to AWS Console -> IAM -> User (ex. iac) -> Assigned MFA device -> Manage -> Virtual MFA Devices -> Scan QR code = You have successfully assigned virtual MFA 
- Copy the MFA ARN: arn:aws:iam::acc_id:mfa/iac (Virtual)  to [profile iac]

### PROFILE
[profile iac]
region=us-east-1
output=json
mfa_serial= arn:aws:iam::acc_id:mfa/iac (Virtual) 

# Use AWS-VAULT
- aws-vault.exe exec ksanchez --duration=12h
- aws-vault.exe exec iac --duration=12h -- cmd.exe

# Running Terraform from Docker-compose
- docker compose -f .\deploy\docker-compose.yml run --rm terraform init



